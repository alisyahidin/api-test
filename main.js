const express = require('express');
const app = express();
const axios = require('axios');

app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const url = 'https://rsks.antrein.com/online/faq';

    axios.get(url).then((response) => {
        const data = response.data;

        res.send(data);
        console.log(data);
    });
});

app.listen(3000, () => { console.log('listening on port 3000') });
